jQuery(document).ready(function(){
	
	jQuery(this).scroll(function(){
		var x = jQuery(this).scrollTop();
		
		if (x >= 85) {
			jQuery('#mainnav').addClass('navbar-fixed-top');
			jQuery('.responsive-menu-button').addClass('fixed-top');
		} else {
			jQuery('#mainnav').removeClass('navbar-fixed-top');
			jQuery('.responsive-menu-button').removeClass('fixed-top');
		}
		
		if (x >= 400) {
			jQuery('.headerwrapper').addClass('breadcrumb-fixed-top');
		} else {
			jQuery('.headerwrapper').removeClass('breadcrumb-fixed-top');
		}
		
		if (x >= 460) {
			jQuery('.sticky-in-breadcrumb').addClass('btn-fixed-top');
		} else {
			jQuery('.sticky-in-breadcrumb').removeClass('btn-fixed-top');
		}

	});
	
	jQuery('.dropdown-menu a').click(function(){
  	jQuery('#selected').text(jQuery(this).text());
	});
	
	
	jQuery("#showmenu").hover(
		function() {
			jQuery("#overmenu").addClass("open");
		}, function() {
			jQuery("#overmenu").removeClass("open");
		}
	);
	
	jQuery("#overmenu").hover(
		function() {
			jQuery("#overmenu").addClass("open");
		}, function() {
			jQuery("#overmenu").removeClass("open");
		}
	);
	
	
	// FAQ
	jQuery( ".faq-title" ).click(function() {
		
		if (jQuery(this).hasClass('active')) {
			jQuery('.faq-title').removeClass('active');
  		jQuery('.faq-answer').removeClass('active');
		} else {
			jQuery('.faq-title').removeClass('active');
  		jQuery('.faq-answer').removeClass('active');
			jQuery(this).addClass('active');
  		jQuery(this.nextElementSibling).addClass('active');
		}
  	
	});
	
});