# unity
https://unity.lf4.nl landing page cloned. 
Alleen de hoofd html pagina. En alle css en js.  
Geen volledige wordpress clone. 
Backend wel toegankelijk via backend_api.json.

**Nerds:**  
Clone repo & create je eigen branch.

**Gewone mensen:**  
Op de 'Project' pagina (linksboven), iets onder de blauwe knop 'clone' (rechtsboven),  
staat een download knop (icon: wolkje). Download als zip, unzip en open unity.html